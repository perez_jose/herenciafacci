package herenciainstrumentos;


import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class DirectorDeOrquesta {
    private String Nombre;
    private String Apellidos;
    private String Apodos;
    private String Credenciales;
    
    private List<Musicos> ListaDeMusicos;

    public DirectorDeOrquesta() {
        ListaDeMusicos = new ArrayList<>();
            
        
    }
    public void iniciarOrquesta (Musicos musico){
        Random numerodeCredenciales =new Random();
        musico.setCredenciales(String.valueOf(numerodeCredenciales.nextInt()));
        ListaDeMusicos.add(musico);
     
    }
    
    
    
    
    
  public void DirijirOrquesta(){
    for (Musicos objetoMusicos : ListaDeMusicos){
        System.out.println(objetoMusicos.getNombre());
         System.out.println(objetoMusicos.getCredenciales());
    }  
  }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    public String getApellidos() {
        return Apellidos;
    }

    public void setApellidos(String Apellidos) {
        this.Apellidos = Apellidos;
    }

    public String getApodos() {
        return Apodos;
    }

    public void setApodos(String Apodos) {
        this.Apodos = Apodos;
    }

    public String getCredenciales() {
        return Credenciales;
    }

    public void setCredenciales(String Credenciales) {
        this.Credenciales = Credenciales;
    }
  
}
