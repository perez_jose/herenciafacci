package compradegasolina;

//registro del cliente
public class Clientes {
    private String Nombre;
    private String Apellido;
    private int Cedula;
    private String Direccion;
    private int GasolinaSuper=2;
    private Double GasolinaExtra=1.50;
    private int CantidadDeGalones;
    private int Iva=12/100;
 
    
    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    public String getApellido() {
        return Apellido;
    }

    public void setApellido(String Apellido) {
        this.Apellido = Apellido;
    }

    public int getCedula() {
        return Cedula;
    }

    public void setCedula(int Cedula) {
        this.Cedula = Cedula;
    }

    public String getDireccion() {
        return Direccion;
    }

    public void setDireccion(String Direccion) {
        this.Direccion = Direccion;
    }
       public int getGasolinaSuper() {
        return GasolinaSuper;
    }

    public void setGasolinaSuper() {
        this.GasolinaSuper = 2;
    }

    public Double getGasolinaExtra() {
        return GasolinaExtra;
    }

    public void setGasolinaExtra() {
        this.GasolinaExtra = 1.50;
    }

    public int getCantidadDeGalones() {
        return CantidadDeGalones;
    }

    public void setCantidadDeGalones(int CantidadDeGalones) {
        this.CantidadDeGalones = CantidadDeGalones;
    }

    public int getIva() {
        return Iva;
    }

    public void setIva() {
        this.Iva = 12/100;
    }
    public double SubtotalExtra(){
        return this.GasolinaExtra *this.CantidadDeGalones;
    }

   
public double TotalExtra(){
    return this.SubtotalExtra()*this.Iva;
}  
}
